ruamel.yaml.clib (0.2.12+ds-1) unstable; urgency=medium

  * d/control: Removed "Multi-Arch: same" due to file conflicts.
  * New upstream version.
  * Standards-Version: 4.7.0 (routine-update)
  * Removed s390x patch that was applied upstream. The other patch was also
    removed as it is no longer needed.
  * d/copyright: update my email address

 -- Michael R. Crusoe <crusoe@debian.org>  Wed, 22 Jan 2025 10:56:50 +0100

ruamel.yaml.clib (0.2.8+ds-2) unstable; urgency=medium

  * d/patches/fix-typecasts-s390x.patch: adopted from Fedora to fix
    FTBFS with GCC 14. Closes: #1075453
  * Standards-Version: 4.6.2 (routine-update)
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Wed, 31 Jul 2024 19:00:29 +0200

ruamel.yaml.clib (0.2.8+ds-1) unstable; urgency=medium

  * Team upload
  * Download source from SourceForge, including Cython files
  * Reintroduce build-dependency on cython3, and rebuild C file from
    source before compiling

 -- Julian Gilbey <jdg@debian.org>  Thu, 18 Apr 2024 05:34:49 +0100

ruamel.yaml.clib (0.2.8-2) unstable; urgency=medium

  * Team upload
  * Update Debian autopkgtests for newer version of ruamel.yaml
  * Bump Standards-Version to 4.7.0, no changes needed
  * Drop unnecessary build-dependency on cython3

 -- Julian Gilbey <jdg@debian.org>  Mon, 15 Apr 2024 13:55:43 +0100

ruamel.yaml.clib (0.2.8-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version
    - compatibility with Python 3.12 (Closes: #1055724)
  * Update standards version to 4.6.2, no changes needed.
  * Use pybuild-plugin-pyproject with build system.
  * Fix up lintian-overrides for new format.
  * Add simple autopkgtest to test that clib is built, loads and is minimally
    functinoal.

 -- Stuart Prescott <stuart@debian.org>  Sat, 09 Dec 2023 19:01:39 +1100

ruamel.yaml.clib (0.2.7-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Sat, 29 Oct 2022 18:49:45 +0200

ruamel.yaml.clib (0.2.6-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Update standards version to 4.5.1, no changes needed.

  [ Michael R. Crusoe ]
  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Fri, 22 Oct 2021 14:15:45 +0200

ruamel.yaml.clib (0.2.2-1) unstable; urgency=medium

  [ Michael R. Crusoe ]
  * Mark the binary package as Multi-Arch: same

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Michael R. Crusoe ]
  * New upstream version
  * debhelper-compat 13 (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Thu, 08 Oct 2020 14:56:56 +0200

ruamel.yaml.clib (0.2.0-3) unstable; urgency=medium

  * autopkgtest-pkg-python doesn't work with namespace packages, so don't use
    it.

 -- Michael R. Crusoe <crusoe@debian.org>  Thu, 18 Jun 2020 16:17:23 +0200

ruamel.yaml.clib (0.2.0-2) unstable; urgency=medium

  * Testsuite: autopkgtest-pkg-python (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Breaks+Replaces ruamel.yaml before 0.16.10 (Closes: #962971)

 -- Michael R. Crusoe <crusoe@debian.org>  Wed, 17 Jun 2020 10:22:10 +0200

ruamel.yaml.clib (0.2.0-1) unstable; urgency=medium

  * Initial packaging (Closes: #955282)

 -- Michael R. Crusoe <crusoe@debian.org>  Thu, 04 Jun 2020 13:31:19 +0200
